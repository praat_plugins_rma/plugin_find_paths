# Search for folders in a directory hierarchy
#
# Written by Rolando Muñoz A. (08 Sep 2017)
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.
#
form Find directories
  sentence Directory /home/rolando/Music
  word Name *
  integer Depth -1
endform

string = Create Strings as tokens: "", " ,"
Rename: "directoryList_traverse"

runScript: "find_directories_call.praat", string, directory$, name$, 0, depth

selectObject: string