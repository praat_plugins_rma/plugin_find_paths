# Search for folders in a directory hierarchy. It uses the Depth-first Search algorithm
#
# Written by Rolando Muñoz A. (08 Sep 2017)
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.
#
form Find directories
  natural String_ID 
  sentence Directory 
  word Name *
  integer Depth_i 0
  integer Depth -1
endform

# Check depth
if depth >= 0
  if depth_i > depth
    exitScript()
  endif
  depth_i += 1
endif

# Add to String object
selectObject: string_ID
Insert string: 0, directory$

directoryList = Create Strings as directory list: "directoryList", directory$ + "/" + name$
nDirectories= Get number of strings

for i to nDirectories
  folder_name$ = object$[directoryList, i]
  runScript: "find_directories_call.praat", string_ID, directory$ + "/" + folder_name$ ,name$, depth_i, depth
endfor

removeObject: directoryList