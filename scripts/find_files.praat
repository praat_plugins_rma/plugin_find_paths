# Search for files in a directory hierarchy
#
# Written by Rolando Muñoz A. (08 Sep 2017)
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.
#
form Create Strings as file list (recursively)
  sentence Directory /home/rolando/Music
  word Name *
  integer Depth -1
endform

string_Directories = Create Strings as tokens: "", " ,"
Rename: "directoryList_traversal"
string_Files = Create Strings as tokens: "", " ,"
Rename: "fileList_traversal"

runScript: "find_directories_call.praat", string_Directories, directory$, "*", 0, depth

selectObject: string_Directories
n = Get number of strings
for i to n
  selectObject: string_Directories
  directory$ = object$[string_Directories, i]
  fileList = Create Strings as file list: "fileList", directory$ + "/" + name$
  m = Get number of strings
  selectObject: string_Files
  for j to m
    filename$ = object$[fileList, j]
    Insert string: 0, directory$ + "/"+ filename$
  endfor
  removeObject: fileList
endfor

removeObject: string_Directories
selectObject: string_Files